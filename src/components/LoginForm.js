import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import ErrorMessage from "./ErrorMessage";

const LoginForm = ({ url, handleLoginSuccess }) => {
  const [error, setError] = useState({
    exists: false,
    message: null,
  });

  const [userInfo, setUserInfo] = useState({
    email: null,
    password: null,
  });

  const history = useHistory();

  const handleChange = (e) => {
    setUserInfo({
      ...userInfo,
      [e.target.name]: e.target.value,
    });
  };

  const handleLogin = (e) => {
    e.preventDefault();

    fetch(url + "/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(userInfo),
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        if (data.error) {
          setError({
            exists: true,
            action: "Login",
            message: data.error,
          });
        } else {
          console.log(data);
          setError({
            exists: false,
            message: false,
          });
          //   setSuccess({
          //     exists: true,
          //     action: "Login",
          //   });
          window.localStorage.setItem("token", "Bearer " + data.token);
          history.push("/");
          handleLoginSuccess(data.user);
        }
      });
  };

  return (
    <React.Fragment>
      <div className="col-12">
        {error.message && <ErrorMessage error={error} />}
      </div>
      <div className="col-12">
        <form onSubmit={handleLogin}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email address</label>
            <input
              name="email"
              type="email"
              className="form-control"
              onChange={handleChange}
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={handleChange}
              id="exampleInputPassword1"
            />
          </div>

          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </form>
      </div>
    </React.Fragment>
  );
};

export default LoginForm;
