import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = ({ user, handleLogout }) => {
  return (
    <div>
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <NavLink exact to="/" className="navbar-brand">
            Elite Talent
          </NavLink>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <NavLink exact to="/" className="nav-link">
                  Home
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink exact to="/audition" className="nav-link">
                  Audition Now
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink exact to="/gallery" className="nav-link">
                  Gallery
                </NavLink>
              </li>
              {/* <li className="nav-item">
                <NavLink exact to="/about" className="nav-link">
                  About Us
                </NavLink>
              </li> */}
              <li className="nav-item">
                <NavLink exact to="/contact" className="nav-link">
                  Contact Us
                </NavLink>
              </li>
            </ul>

            <ul className="navbar-nav ml-auto">
              <span className="navbar-text mr-3">+(02) 812 3456</span>
              <span className="navbar-text mr-3">
                admissions@elitetalents.com
              </span>

              {user._id ? (
                <React.Fragment>
                  <li className="nav-item active">
                    <NavLink exact to="/login" className="nav-link">
                      {user.firstname}
                    </NavLink>
                  </li>

                  <li className="nav-item active">
                    <NavLink
                      exact
                      to="/"
                      className="nav-link"
                      onClick={handleLogout}
                    >
                      Logout
                    </NavLink>
                  </li>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <li className="nav-item active">
                    <NavLink exact to="/login" className="nav-link">
                      Login
                    </NavLink>
                  </li>

                  <li className="nav-item active">
                    <NavLink exact to="/register" className="nav-link">
                      Register
                    </NavLink>
                  </li>
                </React.Fragment>
              )}
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
