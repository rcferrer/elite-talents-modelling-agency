import React, { useState, useEffect } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import NoMatch from "./pages/NoMatch";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

import Home from "./pages/Home";
import Audition from "./pages/Audition";
import Gallery from "./pages/Gallery";
import Login from "./pages/Login";
import Register from "./pages/Register";
import About from "./pages/About";
import Contact from "./pages/Contact";

function App() {
  const url = "https://elite-talent-backend.herokuapp.com";

  const [user, setUser] = useState({
    _id: null,
    firstname: null,
    lastname: null,
    email: null,
    image: null,
    isAdmin: null,
  });

  useEffect(() => {
    if (window.localStorage.getItem("token")) {
      fetch(url + "/users/profile", {
        headers: {
          Authorization: window.localStorage.getItem("token"),
        },
      })
        .then((response) => {
          if (response.status === 401) {
            return { error: "Unauthorized" };
          } else {
            return response.json();
          }
        })
        .then((data) => {
          if (data) {
            if (!data.error) {
              setUser(data);
            }
          }
        });
    }
  }, []);

  const handleLoginSuccess = (userInfo) => {
    setUser(userInfo);
  };

  const handleLogout = () => {
    setUser({
      _id: null,
      firstname: null,
      lastname: null,
      email: null,
      image: null,
      isAdmin: null,
    });
    window.localStorage.removeItem("token");
  };

  return (
    <React.Fragment>
      <Router>
        <Navbar user={user} handleLogout={handleLogout} />
        <Switch>
          <Route exact path="/" component={Home} />

          <Route exact path="/audition">
            <Audition url={url} />
          </Route>

          <Route exact path="/gallery">
            <Gallery url={url} />
          </Route>

          <Route exact path="/login">
            <Login url={url} handleLoginSuccess={handleLoginSuccess} />
          </Route>

          <Route exact path="/register">
            <Register
              url={url}
              user={user}
              handleLoginSuccess={handleLoginSuccess}
            />
          </Route>

          <Route exact path="/about">
            <About />
          </Route>

          <Route exact path="/contact">
            <Contact />
          </Route>

          <Route>
            <NoMatch />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </React.Fragment>
  );
}

export default App;
