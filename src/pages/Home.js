import React from "react";
import { NavLink } from "react-router-dom";
import testimonial1 from "./../images/testimonial1.png";
import testimonial2 from "./../images/testimonial2.png";
import testimonial3 from "./../images/testimonial3.png";

const Home = () => {
  return (
    <React.Fragment>
      <div className="container-fluid px-0">
        <div className="col-12 banner d-flex justify-content-center py-5">
          <div className="col-10">
            <h1 className="display-4 banner-text">We are looking for</h1>
            <h1 className="display-4 banner-text">
              <strong>Runway Models</strong>
            </h1>
            <h5 className="banner-text">
              Be one of our talents for Chanel Fashion Show 2020
            </h5>
            <NavLink to="/audition" className="btn btn-lg btn-success my-3">
              AUDITION NOW
            </NavLink>
          </div>
        </div>
      </div>
      <div className="container pb-5">
        <div className="row">
          <div className="col-12 pt-5">
            <h1 className="text-center mb-5">
              Get the best models for your fashion event
            </h1>
          </div>
          <div className="col-12 d-flex flex-wrap pb-5">
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F1171558146%2F960x0.jpg%3Ffit%3Dscale&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Runway Contest, PH 2018
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.hawtcelebs.com%2Fwp-content%2Fuploads%2F2018%2F02%2Fgigi-hadid-at-versace-runway-show-at-milan-fashion-week-02-23-2018-16.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    ACF Fashion Show, Singapore 2019
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-4 col-sm-12">
              <div className="card">
                <img
                  src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.theskinnybeep.com%2Fwp-content%2Fuploads%2F2016%2F01%2FCalvin-Klein-Spring-Summer-2016-Womenswear-Collection-London-Fashion-Week-Fashion-Show.jpg&f=1&nofb=1"
                  className="card-img-top gallery"
                  alt="..."
                />
                <div class="card-body">
                  <p class="card-text text-center">
                    Guess Fashion Show, PH 2019
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid testimonials py-5">
        <div className="row">
          <div className="col-12 d-flex justify-content-center">
            <div className="col-6 testimonial">
              <div
                id="carouselExampleSlidesOnly"
                class="carousel slide"
                data-ride="carousel"
              >
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src={testimonial1} class="d-block w-100" alt="..." />
                  </div>
                  <div class="carousel-item">
                    <img src={testimonial2} class="d-block w-100" alt="..." />
                  </div>
                  <div class="carousel-item">
                    <img src={testimonial3} class="d-block w-100" alt="..." />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Home;
