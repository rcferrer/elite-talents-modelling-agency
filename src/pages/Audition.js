import React from "react";
import AuditionForm from "../components/AuditionForm";

const Audition = ({ url }) => {
  return (
    <div className="container audition my-5">
      <div className="row">
        <div className="col-12">
          <h1 className="text-center">Audition Form</h1>
        </div>
        <AuditionForm url={url} />
      </div>
    </div>
  );
};

export default Audition;
