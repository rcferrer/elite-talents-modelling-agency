import React from "react";
import LoginForm from "../components/LoginForm";

const Login = ({ url, handleLoginSuccess }) => {
  return (
    <div className="container my-5 main-view">
      <div className="row">
        <div className="col-12">
          <h1 className="text-center">Login</h1>
        </div>
        <LoginForm url={url} handleLoginSuccess={handleLoginSuccess} />
      </div>
    </div>
  );
};

export default Login;
