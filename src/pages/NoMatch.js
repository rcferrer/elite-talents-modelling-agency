import React from "react";

const NoMatch = () => {
  return <div>The page you're looking for does not exist.</div>;
};

export default NoMatch;
