import React from "react";

const Contact = () => {
  return (
    <div className="container my-5 main-view">
      <div className="row">
        <div className="col-lg-6 col-md-12 col-sm-12 px-4">
          <h2>Contact Us</h2>
          <h5>Message us for partnerships and business opportunities.</h5>
          <hr />

          <p>Contact No: +(02) 812 3456 </p>
          <p>Email address: admissions@elitetalents.com</p>
          <p>
            Address: Elite Talent Bldg., Ronquillo St., Sta. Cruz, Manila,
            Philippines 1559
          </p>
        </div>
        <div className="col-lg-6 col-md-12 col-sm-12 px-4">
          <iframe
            key="iframe"
            width="100%"
            height="400"
            frameBorder="0"
            src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Manila%2C%20NCR%2C%20Philippines+(Catch%20Up%20Learning)&amp;ie=UTF8&amp;t=&amp;z=16&amp;iwloc=B&amp;output=embed"
          >
            <a href="https://www.maps.ie/draw-radius-circle-map/">
              km radius map
            </a>
          </iframe>
        </div>
      </div>
    </div>
  );
};

export default Contact;
